(function () {
    "use strict";
    var _document = document,
            calories_elem, sumCal = 0;

    function reset(e) {
        var _focused = _document.getElementsByClassName('focused');
        var pos = _focused.length;
        while (pos--) {
            while (_focused[pos].firstChild) {
                _focused[pos].removeChild(_focused[pos].firstChild);
            }
            _focused[pos].classList.toggle('focused');
        }
        if (calories_elem) {
            calories_elem.value = calories_elem.getAttribute('value');
        }
        sumCal = 0;
    }

    function checkIt(evt) {
        evt = evt ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function getItems() {
        return {
            collection: [{
                    name: "Download Ninja",
                    img: "http://organic-food.bold-themes.com/main-demo/wp-content/uploads/sites/3/2016/07/products_10-300x300.jpg",
                    coefficient: 25
                }, {
                    name: "Standard Quality",
                    img: "http://organic-food.bold-themes.com/main-demo/wp-content/uploads/sites/3/2016/07/products_13-300x300.jpg",
                    coefficient: 51
                }, {
                    name: "Woo Single #2",
                    img: "http://organic-food.bold-themes.com/main-demo/wp-content/uploads/sites/3/2016/07/products_11-300x300.jpg",
                    coefficient: 15
                }, {
                    name: "Woo Premium",
                    img: "http://organic-food.bold-themes.com/main-demo/wp-content/uploads/sites/3/2016/07/products_16-300x300.jpg",
                    coefficient: 2
                }, {
                    name: "Virtual Woo Ninja",
                    img: "http://organic-food.bold-themes.com/main-demo/wp-content/uploads/sites/3/2016/07/products_14-300x300.jpg",
                    coefficient: 6
                }, {
                    name: "Standard Ninja",
                    img: "http://organic-food.bold-themes.com/main-demo/wp-content/uploads/sites/3/2016/07/products_04-300x300.jpg",
                    coefficient: 105
                }]
        };
    }

    function buildElement(tag, attrs) {
        var _tag = _document.createElement(tag);
        if (attrs instanceof Object && Object.keys(attrs).length) {
            for (var key in attrs) {
                if (!attrs.hasOwnProperty(key))
                    continue;
                _tag.setAttribute(key, attrs[key]);
            }
        }
        return _tag;
    }

    var product = {

        build: function (params) {

            var item = buildElement('div', {
                "class": "product col-sm-4 col-md-3"
            });
            var _img = buildElement('div', {
                "class": "product-image"
            });
            _img.style.backgroundImage = 'url(' + params.img + ')';
            var _text = this.title(params.name);
            var _wraper = this.wraper();
            _wraper.onclick = function (e) {
                click_1(e, params.coefficient);
            };


            item.appendChild(_img);
            item.appendChild(_wraper);
            item.appendChild(_text);

            return item;

            function click_1(e, coef) {
                e.stopPropagation();
                var element = e.currentTarget;
                var emptyInputs = _document.querySelectorAll('.empty');
                if (emptyInputs) {
                    var pos = emptyInputs.length;
                    while (pos--) {
                        var _focused = emptyInputs[pos].parentNode;
                        while (_focused.firstChild) {
                            _focused.removeChild(_focused.firstChild);
                        }
                        _focused.classList.remove('focused');
                    }
                }

                var _addInput = element.querySelector('input');
                if (!_addInput) {
                    var _addInput = product._input(coef);
                    element.appendChild(_addInput);
                }
                sumCal = +calories_elem.value;

                if (_addInput.value) {
                    sumCal = sumCal - _addInput.value * coef;
                }

                element.classList.add('focused');
                _addInput.focus();
            }
        },

        title: function (text) {
            var _p = buildElement('p');
            _p.innerText = text;
            return _p;
        },
        wraper: function () {
            var _div = buildElement('div', {
                "class": "wrap"
            });
            return _div;
        },

        _input: function (coefficient) {
            var _div = buildElement('input', {
                "class": "product_weight_input empty",
                "type": "text",
                "data-coefficient": coefficient,
                "placeholder": "input weight in grams here"
            });

            _div.onkeypress = checkIt;

            _div.oninput = function () {
                var input_value = this.value;
                var coefficient = this.dataset.coefficient;

                var output_value = input_value * coefficient;

                if (!this.value) {
                    this.classList.add('empty');
                } else if (this.classList.contains('empty')) {
                    this.classList.remove('empty');
                }

                if (calories_elem) {
                    calories_elem.value = +sumCal + output_value;
                }
            };

            return _div;
        }
    };

    var container = {
        build: function () {

            var _items = getItems();
            var productsArea = buildElement('div', {
                "id": "productsArea",
                "class": "container"
            });

            for (var i = 0; i < _items.collection.length; i++) {
                var _product = product.build(_items.collection[i]);

                productsArea.appendChild(_product);
            }
            return productsArea;
        }
    };

    var footer = {
        build: function () {
            var _container = buildElement('div', {
                "class": "container footer"
            });
            _container.appendChild(this.input());
            return _container;
        },

        input: function () {
            function showResult(e) {
                if (calories_elem && calories_elem.value) {
                    var inputs = _document.getElementsByClassName('product_weight_input');
                    var sumCal = 0;
                    for (var i = 0; i < inputs.length; i++) {
                        sumCal += +inputs[i].value;
                    }
                    alert(sumCal + " grams of this nutrient contains " + calories_elem.value + " calories.");
                }
            }

            var _div = buildElement('div', {
                "class": "col-xs-12 col-sm-offset-2 col-sm-8"
            });
            calories_elem = buildElement('input', {
                "id": "calculate_calories",
                "disabled": "disabled",
                "type": "text",
                "placeholder": "0"
            });
            calories_elem.value = 0;

            var _text = buildElement('span', {
                "class": "text"
            });
            _text.innerText = 'Calories';

            var _button = buildElement('button', {
                "class": "btn btn-success send"
            });
            _button.innerText = 'Send';
            _button.onclick = showResult;

            var _reset = buildElement('button', {
                "class": "btn btn-success send"
            });
            _reset.innerText = 'Reset';
            _reset.onclick = reset;

            _div.appendChild(calories_elem);
            _div.appendChild(_text);
            _div.appendChild(_button);
            _div.appendChild(_reset);

            return _div;
        }
    };

    _document.body.appendChild(container.build());
    _document.body.appendChild(footer.build());
})();